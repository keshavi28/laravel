@extends('admin.layout.default')
<style>
    .ui-datepicker
    {
        background-color: aliceblue;
    }
    .ui-datepicker-next
    {
        float: right;
    }
</style>
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@section('content')
<div class="row match-height">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-form">Register</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div style="text-align:center;">
                    <form method="POST" action="{{route('admin-data-filter')}}">
                        @csrf
                        <ul class="list-inline mb-0">
                            <li><input type="text" autocomplete="off" class="form-control" id="fromdate" name="from_date" placeholder="From" value="{{session('filter_date_from')}}"></li>
                            <li><input type="text" autocomplete="off" class="form-control" id="todate" name="to_date" placeholder="To" value="{{session('filter_date_to')}}" disabled></li>
                            <li><input type="submit" class="btn btn-warning" value="Apply Filter"></li>
                            @if(session('filter_date_from'))
                            <li><a href="{{route('admin-register')}}" class="btn btn-secondary">Remove Filter</a></li>
                            @endif
                        </ul>
                    </form>
                </div>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                        <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                        <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                        <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="card-body collapse in">
                <div class="card-block">
                    <div class="form-group row add">
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="name" name="name"
                                placeholder="Enter first name" required>
                            <p class="error text-center alert alert-danger hidden"></p>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="last_name" name="last_name"
                                placeholder="Enter last name">
                            <p class="error text-center alert alert-danger hidden"></p>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="email" name="email"
                                placeholder="Enter email">
                            <p class="error text-center alert alert-danger hidden"></p>
                        </div>
                        <div class="col-md-2">
                            <input type="password" class="form-control" id="password" name="password"
                                placeholder="Enter password" required>
                            <p class="error text-center alert alert-danger hidden"></p>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="role" id="role">
                                <option style="display: none;">-- Select Role --</option>
                                    @foreach($all_roles as $key => $row)
                                        <option value="{{$row->id}}">{{$row->role}}</option>
                                    @endforeach
                            </select>
                            <p class="error text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group row add">
                        <div class="col-md-2">
                            <button class="btn btn-primary" type="submit" id="add">
                                <span class="glyphicon glyphicon-plus"></span> ADD
                            </button>
                        </div>
                    </div>
                </div>
                @csrf
                <div class="card-block">
                    <div class="table-responsive text-center">
                        <table class="table table-borderless" id="table">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">First Name</th>
                                    <th class="text-center">Last Name</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Role</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            @foreach($reg as $item)
                            <tr class="item{{$item->id}}">
                                <td>{{$item->id}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->last_name}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->getUserRole->getRole->role}}</td>
                                <td><button class="edit-modal btn btn-info" data-id="{{$item->id}}"
                                data-name="{{$item->name}}" data-last_name="{{$item->last_name}}">
                                        <span class="glyphicon glyphicon-edit"></span> Edit
                                    </button>
                                    @if($role->role == '3')
                                    <button class="delete-modal btn btn-danger"
                                        data-id="{{$item->id}}" data-name="{{$item->name}}">
                                        <span class="glyphicon glyphicon-trash"></span> Delete
                                    </button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    {{-- <div style="text-align: center;">{{$reg->links()}}</div> --}}
                </div>
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="id">ID:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="fid" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="name">First Name:</label>
                                        <div class="col-sm-8">
                                            <input type="name" class="form-control" id="f_n">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="last_name">Last Name:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="l_n">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="role">Role:</label>
                                        <div class="col-sm-8">
                                            {{-- <select class="form-control" name="role" id="role_edit"> --}}
                                                {{-- <option value="user">User</option>
                                                <option value="admin">Admin</option>
                                                <option value="manager">Manager</option>
                                                <option value="superadmin">Super Admin</option>
                                                <option value="supervisor">Supervisor</option> --}}
                                            <select class="form-control" name="role" id="r">
                                                @foreach($all_roles as $key=> $row)
                                                    <option value="{{$row->id}}">{{$row->role}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </form>
                                <div class="deleteContent">
                                    Are you sure you want to delete <span class="dname"></span> ? <span
                                        class="hidden did"></span>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn actionBtn" data-dismiss="modal">
                                        <span id="footer_action_button" class='glyphicon'> </span>
                                    </button>
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">
                                        <span class='glyphicon glyphicon-remove'></span> Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer-js')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#preview')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {

    $(document).on('click', '.edit-modal', function() {
        console.log($(this).data())
            $('#footer_action_button').text("Update");
            $('#footer_action_button').addClass('glyphicon-check');
            $('#footer_action_button').removeClass('glyphicon-trash');
            $('.actionBtn').addClass('btn-success');
            $('.actionBtn').removeClass('btn-danger');
            $('.actionBtn').addClass('edit');
            $('.modal-title').text('Edit');
            $('.deleteContent').hide();
            $('.form-horizontal').show();
            $('#fid').val($(this).data('id'));
            $('#f_n').val($(this).data('name'));
            $('#l_n').val($(this).data('last_name'));
            // $('#r').val($(this).data('role'));
            // var option ='<option value="value_here">data_here</option>';
            // var option = '<?php foreach($reg as $val) { echo $val->getUserRole->role; } ?>';
            // alert(option);
            $('#r').val(3);
            $('#myModal').modal('show');
        });
        $(document).on('click', '.delete-modal', function() {
            $('#footer_action_button').text(" Delete");
            $('#footer_action_button').removeClass('glyphicon-check');
            $('#footer_action_button').addClass('glyphicon-trash');
            $('.actionBtn').removeClass('btn-success');
            $('.actionBtn').addClass('btn-danger');
            $('.actionBtn').addClass('delete');
            $('.modal-title').text('Delete');
            $('.did').text($(this).data('id'));
            $('.deleteContent').show();
            $('.form-horizontal').hide();
            $('.dname').html($(this).data('name'));
            $('#myModal').modal('show');
        });

        $('.modal-footer').on('click', '.edit', function() {
            $.ajax({
                type: 'post',
                url: '/new_project/admin/editItem',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $("#fid").val(),
                    'name': $('#f_n').val(),
                    'last_name': $('#l_n').val(),
                    'role': $('#r').val()
                },
                success: function(data) {
                    console.log(data)
                    if(data[1].role=='1')
                    {
                        var role = 'Admin';
                    }
                    else if(data[1].role=='2')
                    {
                        var role = 'User';
                    }
                    else if(data[1].role=='3')
                    {
                        var role = 'Super Admin';
                    }
                    else if(data[1].role=='4')
                    {
                        var role = 'Supervisor';
                    }
                    else if(data[1].role=='5')
                    {
                        var role = 'Manager';
                    }
                    else
                    {
                        var role='No Role';
                    }
                    $('.item' + data[0].id).replaceWith("<tr class='item" + data[[0]].id + "'><td>" + data[0].id + "</td><td>" + data[0].name + "</td><td>" + data[0].last_name + "</td><td>" + data[0].email + "</td><td>" + role + "</td><td><button class='edit-modal btn btn-info' data-last_name='" + data[0].last_name +"' data-id='" + data[0].id + "' data-name='" + data[0].name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data[0].id + "' data-name='" + data[0].name + "' ><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
                }
            });
        });
        $("#add").click(function()
        {
            const name = document.querySelector('input[name=name]')
            if (name.value.trim() === '')
            {
                return false
            }
            else
            {
                $.ajax({
                    type: 'post',
                    url: '/new_project/admin/addItem',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'name': $('input[name=name]').val(),
                        'last_name': $('input[name=last_name]').val(),
                        'email': $('input[name=email]').val(),
                        'password':$('input[name=password]').val(),
                        'role':$('#role').val()
                    },
                    success: function(data) {
                        console.log(data)
                        if ((data.errors)){
                        $('.error').removeClass('hidden');
                            $('.error').text(data.errors.name);
                        }
                        else {
                            $('.error').addClass('hidden');
                            if(data[1]=='3'){
                                var delete_btn = "<button class='delete-modal btn btn-danger' data-id='" + data[0].id + "' data-name='" + data[0].name + "'><span class='glyphicon glyphicon-trash'></span> Delete</button>";
                            }else{
                                var delete_btn='';
                            }
                            $('#table').append("<tr class='item" + data[0].id + "'><td>" + data[0].id + "</td><td>" + data[0].name + "</td><td>" + data[0].last_name + "</td><td>" + data[0].email + "</td><td></td><td><button class='edit-modal btn btn-info' data-id='" + data[0].id + "' data-name='" + data[0].name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button>  "+delete_btn+" </td></tr>");
                        }
                    },

                });
            }

            $('#name').val('');
            $('#last_name').val('');
            $('#email').val('');
            $('#password').val('');
            $('#role').val('-- Select Role --');
        });
        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                type: 'post',
                url: '/new_project/admin/deleteItem',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $('.did').text()
                },
                success: function(data) {
                    $('.item' + $('.did').text()).remove();
                }
            });
        });
    });
</script>
{{-- <script src="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"></script> --}}
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">

    $(function() {
        var currentYear = (new Date).getFullYear();
        var currentMonth = (new Date).getMonth();
        var currentDay = (new Date).getDate();


        $("#fromdate").datepicker({
            minDate: new Date((currentYear - 1), 12, 1),
            dateFormat: 'yy-mm-dd',
            maxDate: new Date(currentYear, currentMonth, currentDay),
            onClose: function (selectedDate) {
                $('#todate').datepicker("option", "minDate", selectedDate)
                $('#todate').removeAttr("disabled");
                $('#todate').datepicker("refresh");
            }
        });


        $("#todate").datepicker({
            minDate: new Date(currentYear, currentMonth, currentDay),
            dateFormat: 'yy-mm-dd',
            maxDate: new Date(currentYear, currentMonth, currentDay)
        });
    });

</script>
@endsection
