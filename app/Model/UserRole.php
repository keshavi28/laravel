<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'user_roles';
    protected $fillable = ['user_id','role'];
    public function getRole()
    {
        return $this->belongsTo('App\Model\Role','role');
    }
}
