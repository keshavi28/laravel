<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    public function getUserRole()
    {
        return $this->hasOne('App\Model\UserRole','user_id');
    }
}
