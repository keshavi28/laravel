<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use App\Model\Registration;
use App\Model\UserRole;
use App\Model\Role;
use Session;
use Validator;
use DB;

class RegistrationController extends Controller
{
    public function addItem(Request $request)
    {
        $user = Registration::where('email',session('user'))->first();
        $role = UserRole::where('user_id',$user->id)->first();
        // dd('test');
        $data = new Registration;
        $data->name = $request->name;
        $data->last_name = $request->last_name;
        $data->email = $request->email;
        $data->password = Crypt::encrypt($request->password);
        $data->created_by = $user->id;
        $data->save();

        $user_role_data = new UserRole;
        $user_role_data->user_id = $data->id;
        $user_role_data->role = $request->role;
        $user_role_data->save();
        $user_role = $role->role;
        // dd($role->role);
        // return response()->json($data);
        return response()->json(array($data, $user_role));
    }
    public function readItems(Request $request)
    {
        $user = Registration::where('email',session('user'))->with('getUserRole')->first();
        $all_roles = Role::get();
        $role = UserRole::where('user_id',$user->id)->first();
        if($role->role != '3')
        {
            $reg = Registration::where('created_by',$user->id)->with('getUserRole.getRole')->get();
        }
        else
        {
            $reg = Registration::with('getUserRole.getRole')->get();
        }
        $data = compact('reg','user','role','all_roles');
        // dd($reg);
        Session::forget('filter_date_from');
        Session::forget('filter_date_to');
        // dd($reg);
        return view('admin.pages.registrations.add', $data);
    }
    public function editItem(Request $request)
    {
        $data = Registration::with('getUserRole.getRole')->find($request->id);
        // dd($data);
        $data->name = $request->name;
        $data->last_name = $request->last_name;
        $data->save();
        $role_data = UserRole::where('user_id',$request->id)->first();
        $role_data->role = $request->role;
        $role_data->save();
        // dd($role_data);
        return response()->json([$data, $role_data]);
    }
    public function deleteItem(Request $request)
    {
        Registration::find($request->id)->delete();
        UserRole::where('user_id',$request->id)->delete();
        return response()->json();
    }
    public function filter(Request $request)
    {
        $all_roles = Role::get();
        session()->put('filter_date_from',$request->from_date);
        session()->put('filter_date_to',$request->to_date);
        // dd($request->from_date, $request->to_date);
        $user = Registration::where('email',session('user'))->first();
        $role = UserRole::where('user_id',$user->id)->first();
        if($role->role != '3')
        {
            if($request->to_date == '')
            {
                $reg = Registration::whereDate('created_at', $request->from_date)->where('created_by',$user->id)->with('getUserRole.getRole')->get();
            }
            else
            {
                $reg = Registration::whereBetween(DB::raw('DATE(created_at)'), [$request->from_date, $request->to_date])->where('created_by',$user->id)->with('getUserRole.getRole')->get();
            }
        }
        else
        {
            if($request->to_date == '')
            {
                $reg = Registration::whereDate('created_at', $request->from_date)->get();
            }
            else
            {
                $reg = Registration::whereBetween(DB::raw('DATE(created_at)'), [$request->from_date, $request->to_date])->get();
            }
        }
        $data = compact('reg','user','role','all_roles');
        return view('admin.pages.registrations.add',$data);
    }
}
